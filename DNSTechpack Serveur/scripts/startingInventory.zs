mods.initialinventory.InvHandler.addStartingItem(<akashictome:tome>.withTag({"akashictome:data": {industrialforegoing: {id: "industrialforegoing:book_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "industrialforegoing"}, Damage: 0 as short}, rftoolscontrol: {id: "rftoolscontrol:rftoolscontrol_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "rftoolscontrol"}, Damage: 0 as short}, tconstruct: {id: "tconstruct:book", Count: 1 as byte, tag: {"akashictome:definedMod": "tconstruct"}, Damage: 0 as short}, deepresonance: {id: "deepresonance:dr_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "deepresonance"}, Damage: 0 as short}, astralsorcery: {id: "astralsorcery:itemjournal", Count: 1 as byte, tag: {"akashictome:definedMod": "astralsorcery"}, Damage: 0 as short}, theoneprobe: {id: "theoneprobe:probenote", Count: 1 as byte, tag: {"akashictome:displayName": "The One Probe Read Me", "akashictome:definedMod": "theoneprobe", display: {Name: "Akashic Tome (The One Probe Read Me)"}}, Damage: 0 as short}, immersiveengineering: {id: "immersiveengineering:tool", Count: 1 as byte, tag: {"akashictome:definedMod": "immersiveengineering"}, Damage: 3 as short}, embers: {id: "embers:codex", Count: 1 as byte, tag: {"akashictome:definedMod": "embers"}, Damage: 0 as short}, rftools: {id: "rftools:rftools_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "rftools"}, Damage: 0 as short}, rftools1: {id: "rftools:rftools_shape_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "rftools1"}, Damage: 0 as short}, rftools2: {id: "rftoolsdim:rftoolsdim_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "rftools2"}, Damage: 0 as short}, botania: {id: "botania:lexicon", Count: 1 as byte, tag: {"akashictome:definedMod": "botania"}, Damage: 0 as short}, extrautils2: {id: "extrautils2:book", Count: 1 as byte, tag: {"akashictome:definedMod": "extrautils2"}, Damage: 0 as short}, actuallyadditions: {id: "actuallyadditions:item_booklet", Count: 1 as byte, tag: {"akashictome:definedMod": "actuallyadditions"}, Damage: 0 as short}, opencomputers: {id: "opencomputers:tool", Count: 1 as byte, tag: {"akashictome:definedMod": "opencomputers"}, Damage: 4 as short}, integrateddynamics: {id: "integrateddynamics:on_the_dynamics_of_integration", Count: 1 as byte, tag: {"akashictome:definedMod": "integrateddynamics"}, Damage: 0 as short}, techreborn: {id: "techreborn:techmanuel", Count: 1 as byte, tag: {"akashictome:definedMod": "techreborn"}, Damage: 0 as short}, hammercore: {id: "hammercore:manual", Count: 1 as byte, tag: {"akashictome:definedMod": "hammercore"}, Damage: 0 as short}, xnet: {id: "xnet:xnet_manual", Count: 1 as byte, tag: {"akashictome:definedMod": "xnet"}, Damage: 0 as short}, steamworld: {id: "steamworld:manual", Count: 1 as byte, tag: {"akashictome:definedMod": "steamworld"}, Damage: 0 as short}, forestry: {id: "forestry:book_forester", Count: 1 as byte, tag: {"akashictome:definedMod": "forestry"}, Damage: 0 as short}}}), 8);


//====Thermal Foundation (mostly Server-side) Gear Recipe fixes====

//ingots
val iron = <minecraft:iron_ingot>;
val copper = <ore:ingotCopper>;
val tin = <ore:ingotTin>;
val bronze = <ore:ingotBronze>;
val steel = <ore:ingotSteel>;

//gears
val gStone = <ore:gearStone>; //Will be used for steel instead
val gCopper = <thermalfoundation:material:256>;
val gTin = <thermalfoundation:material:257>;
val gBronze = <thermalfoundation:material:291>;
val gSteel = <thermalfoundation:material:288>;

//Remove old recipes
recipes.remove(gSteel, false);
recipes.remove(gCopper, false);
recipes.remove(gTin, false);
recipes.remove(gBronze, false);

//Add recipes

print("Steel Gear Recipe");
recipes.addShaped(gSteel,
	[
		[null, steel, null],
		[steel, gStone, steel],
		[null, steel, null]
	]);

print("Copper Gear Recipe");
recipes.addShaped(gCopper,
	[
		[null, copper, null],
		[copper, iron, copper],
		[null, copper, null]
	]);
	
print("Tin Gear Recipe");
recipes.addShaped(gTin,
	[
		[null, tin, null],
		[tin, iron, tin],
		[null, tin, null]
	]);
	
print("Bronze Gear Recipe");
recipes.addShaped(gBronze,
	[
		[null, bronze, null],
		[bronze, iron, bronze],
		[null, bronze, null]
	]);